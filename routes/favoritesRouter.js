const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Favorites = require('../models/favorites');

const favoritesRouter = express.Router();

favoritesRouter.use(bodyParser.json());

favoritesRouter.route('/')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.find({})
    .populate('user')
    .populate('dishes')
    .then((favorites) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {

    let favorites = [];
    let existingFavorites = [];

    Favorites.findOne({user: req.user._id}, function(err, favorite) {
        if (favorite != undefined && favorite != null) {
            for (i = 0; i < req.body.length; i++) {
                for (k = 0; k < favorite.dishes.length; k++) {
                    if (req.body[i]._id == favorite.dishes[k]) {
                        existingFavorites.push(req.body[i]._id)
                    }
                }
            }

            for (i = 0; i < existingFavorites.length; i++) {
                const result = req.body.findIndex(favorite => favorite._id == existingFavorites[i]);
                req.body.splice(result, 1);
            }
            
            if (req.body === undefined || req.body.length == 0) {
                err = new Error("These dishes have already been added to favorites!");
                err.status = 403;
                return next(err)
            }
            
            for (i = 0; i < req.body.length; i++) {
                favorite.dishes.push(req.body[i]._id)
            } 
            
            favorite.save()
            .then((updatedFavorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(updatedFavorite);                
            }, (err) => next(err));
        } else {
            req.body.user = req.user._id;
            req.body.dishes = req.body;
        
            favorites.push(req.body);
            
            Favorites.create(favorites)
            .then((favorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    });
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id}, function(err, favorite) {
        if (favorite != undefined && favorite != null) {
            favorite.remove({})
        }
    })
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));    
});

favoritesRouter.route('/:dishId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({user: req.user._id})
    .then((favorites) => {
        if (!favorites) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "favorites": favorites});
        }
        else {
            if (favorites.dishes.indexOf(req.params.dishId) < 0) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "favorites": favorites});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "favorites": favorites});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {

    let favoriteIds = [];
    let favorites = [];

    Favorites.findOne({user: req.user._id}, function(err, favorite) {
        if (favorite != undefined && favorite != null) {
            for (i = 0; i < favorite.dishes.length; i++) {
                if ( favorite.dishes[i] == req.params.dishId) {
                    err = new Error("This dish has already been added to favorites!");
                    err.status = 403;
                    return next(err);
                }
            }
            favorite.dishes.push(req.params.dishId)
            favorite.save()
            .then((updatedFavorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(updatedFavorite);                
            }, (err) => next(err));
        } else {
            favoriteIds.push(req.params.dishId);

            req.body.user = req.user._id;
            req.body.dishes = favoriteIds;

            favorites.push(req.body);
    
            Favorites.create(favorites)
            .then((favorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    });
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id}, function(err, favorite) {
        if (favorite != undefined && favorite != null) {
            const result = favorite.dishes.findIndex(favorite => favorite._id == req.params.dishId);
            favorite.dishes.splice(result, 1);
            favorite.save()
            .then((favorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    })
})

module.exports = favoritesRouter;